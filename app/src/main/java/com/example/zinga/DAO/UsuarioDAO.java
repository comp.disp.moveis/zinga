package com.example.zinga.DAO;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.zinga.model.Usuario;
import com.example.zinga.util.SqLite;

import java.util.ArrayList;
import java.util.List;

public class UsuarioDAO {

    private SQLiteDatabase db;

    public UsuarioDAO(SqLite sqlite) {
        db = sqlite.getReadableDatabase();
    }

    public long inserir(Usuario usuario) {
        long resultado;
        ContentValues valores = new ContentValues();
        valores.put("nome", usuario.getNome());
        valores.put("email", usuario.getEmail());
        valores.put("foto", usuario.getFoto());

        resultado = db.insert("usuario", null, valores);

        db.close();
        return resultado;
    }

    public void deletar(String email) {
        db.delete("usuario", "email= ?", new String[]{email});
        db.close();
    }

    public Usuario findWithEmail(String email) {
        Usuario u;
        Cursor cursor = db.rawQuery("SELECT id, nome, email, foto FROM usuario WHERE email = ?", new String[]{email});
        cursor.moveToFirst();

        if (cursor.getCount() > 0) {
            u = new Usuario();
            u.setId(cursor.getInt(0));
            u.setNome(cursor.getString(1));
            u.setEmail(cursor.getString(2));
            u.setFoto(cursor.getBlob(3));
        } else {
            u = null;
        }

        cursor.close();
        db.close();
        return u;
    }


    public List<Usuario> findAll() {
        List<Usuario> list = new ArrayList<>();
        String[] colunas = new String[]{"id", "nome", "email", "foto"};

        Cursor cursor = db.query(
                "usuario",
                colunas,
                null,
                null,
                null,
                null,
                "id ASC");

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();

            do {

                Usuario u = new Usuario();
                u.setId(cursor.getInt(0));
                u.setNome(cursor.getString(1));
                u.setEmail(cursor.getString(2));
                u.setFoto(cursor.getBlob(3));

                list.add(u);

            } while (cursor.moveToNext());
        }
        db.close();
        return list;
    }
}
