package com.example.zinga.controller;

import android.content.ContentResolver;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;

import com.example.zinga.DAO.UsuarioDAO;
import com.example.zinga.model.Usuario;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class UsuarioController {

    public byte[] uriToBlob(Uri uri, ContentResolver contentResolver) {

        Bitmap bitmap = null;
        try {
            bitmap = MediaStore.Images.Media.getBitmap(contentResolver, uri);
        } catch (IOException e) {
            e.printStackTrace();
        }

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 60, outputStream);
        byte[] byteArray = outputStream.toByteArray();

        return byteArray;
    }
    public Bitmap BlobToBitmap(Usuario usuario) {

        try {
            byte[] encodedString = usuario.getFoto();
            Bitmap bitmap = BitmapFactory.decodeByteArray(encodedString, 0, encodedString.length);
            return bitmap;
        } catch (Exception e) {
            e.getMessage();
            return null;
        }
    }

    public boolean insertUser(Usuario user, Context context) {
        UsuarioDAO dao = new UsuarioDAO(new SqLiteController(context).getBanco());
        Long resultado = dao.inserir(user);
        return resultado != null && resultado != 0;
    }

    public Usuario findUser(String email, Context context) {
        UsuarioDAO dao = new UsuarioDAO(new SqLiteController(context).getBanco());
        return dao.findWithEmail(email);
    }
}
