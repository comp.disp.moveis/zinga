package com.example.zinga.controller;

import android.content.Context;

import com.example.zinga.model.Estados;

import org.json.JSONArray;

import java.util.List;

public class EstadosController extends Estados {

    private final Context context;

    public EstadosController(Context context) {
        this.context = context;
    }

    public int getUfEstado(List<Estados> lista, Object obj) {
        int uf = 0;

        for (Estados estados : lista) {
            if (estados.getNome().equals(obj)) {
                uf = estados.getIdentificador();
            }
        }
        return uf;
    }


    public List<Estados> getJsonIterable(JSONArray array) throws Exception {
        return jsonIterable(array);
    }

//    public List<Estados> buscarEstados() {
//        EstadosDAO dao = new EstadosDAO(this.context);
//        return dao.buscar();
//    }
}
