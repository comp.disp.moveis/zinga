package com.example.zinga.controller;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.example.zinga.util.SqLite;

public class SqLiteController {

    private SQLiteDatabase bd;
    private SqLite banco;

    public SqLiteController(Context context) {
        banco = new SqLite(context);
    }

    public SqLite getBanco() {
        return banco;
    }
}
