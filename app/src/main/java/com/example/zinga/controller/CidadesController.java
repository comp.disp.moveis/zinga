package com.example.zinga.controller;

import com.example.zinga.model.Cidades;
import com.example.zinga.model.Estados;

import org.json.JSONArray;

import java.util.List;

public class CidadesController extends Cidades {

    public int getGeocodeCidade(List<Cidades> lista, Object obj) {
        int geocode = 0;

        for (Cidades cidades : lista) {
            if (cidades.getNome().equals(obj)) {
                geocode = cidades.getGeoCode();
            }
        }
        return geocode;
    }

    public List<Cidades> getJsonIterable(JSONArray jsonArray) throws Exception {
        return jsonIterable(jsonArray);
    }
}
