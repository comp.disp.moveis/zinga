package com.example.zinga.util;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class SqLite extends SQLiteOpenHelper {
    public static final String NOME_BANCO = "banco.db";
    public static final String NOME_TABELA = "usuario";
    public static final int VERSAO_BANCO = 1;
    public static final String QUERY_CREATE_USUARIO = "create table usuario(" +
            "id INTEGER primary key autoincrement, " +
            "nome TEXT not null, " +
            "email TEXT not null, " +
            "foto BLOB not null);";

    public SqLite(Context context) {
        super(context, NOME_BANCO, null, VERSAO_BANCO);
    }

    @Override
    public void onCreate(SQLiteDatabase bd) {
        bd.execSQL(QUERY_CREATE_USUARIO);
    }

    @Override
    public void onUpgrade(SQLiteDatabase bd, int oldVersion, int newVersion) {
        bd.execSQL("DROP TABLE IF EXISTS " + NOME_TABELA);
        onCreate(bd);
    }
}
