package com.example.zinga.util;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class HttpService extends AsyncTask<String, String, JSONArray> {

    private final RespostaJson respostaJson;
    private final ProgressDialog progress;
    private final String url;

    public HttpService(RespostaJson respostaJson, ProgressDialog progress, String url) {
        this.respostaJson = respostaJson;
        this.progress = progress;
        this.url = url;
    }

    @Override
    protected void onPreExecute() {
        this.progress.show();
    }

    @Override
    protected JSONArray doInBackground(String... strings) {
        BufferedReader bufferedReader = null;
        StringBuilder stringBuilder;
        JSONArray jsonArray = null;
        HttpURLConnection httpURLConnection = null;

        try {
            URL url = new URL(strings[0]);
            httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setRequestMethod("GET");

            if (httpURLConnection.getResponseCode() != 200) {
                Log.e("Erro na conexão: ", Integer.toString(httpURLConnection.getResponseCode()));
            }

            stringBuilder = new StringBuilder();
            bufferedReader = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));

            String linha;

            while ((linha = bufferedReader.readLine()) != null) {
                stringBuilder.append(linha + "\n");
            }
            if (strings[0].contains("previsao")) {
                JSONObject objPrevisao = new JSONObject(stringBuilder.toString());
                jsonArray = objPrevisao.toJSONArray(objPrevisao.names());

            } else {
                jsonArray = new JSONArray(stringBuilder.toString());
            }

        } catch (Exception e) {
            Log.e("Erro: ", e.getMessage());
            return null;
        } finally {
            if (bufferedReader != null) {
                try {
                    httpURLConnection.disconnect();
                    bufferedReader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return jsonArray;
        }
    }

    @Override
    protected void onPostExecute(JSONArray array) {
        if (!array.equals("")) {
            try {
                respostaJson.atualizaJson(array, url);
            } catch (Exception e) {
                Log.e("Erro: ", e.getMessage());
            }
        }
    }
}
