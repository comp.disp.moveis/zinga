package com.example.zinga.util;

import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.zinga.R;
import com.example.zinga.model.Previsao;

import java.util.List;

public class CustomAdapter extends RecyclerView.Adapter<CustomAdapter.PrevisaoViewHolder> {

    RecyclerView recyclerView;

    public class PrevisaoViewHolder extends RecyclerView.ViewHolder {
        CardView cardRecycler;
        ImageView imgPrevisao;
        TextView textDia;
        TextView textDiaSemana;
        TextView textTurno;
        TextView textTempMax;
        TextView textTempMin;
        TextView textForcaVento;
        TextView textUmidadeMax;
        TextView textUmidadeMin;
        TextView textResumo;

        public PrevisaoViewHolder(View itemView) {
            super(itemView);
            cardRecycler = (CardView) itemView.findViewById(R.id.cardRecycler);
            LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) cardRecycler.getLayoutParams();
            layoutParams.height = recyclerView.getHeight();
            layoutParams.width = recyclerView.getWidth();
            cardRecycler.setLayoutParams(layoutParams);
            imgPrevisao = (ImageView) itemView.findViewById(R.id.imgPrevisao);
            textDia = (TextView) itemView.findViewById(R.id.textDia);
            textDiaSemana = (TextView) itemView.findViewById(R.id.textDiaSemana);
            textTurno = (TextView) itemView.findViewById(R.id.textTurno);
            textTempMax = (TextView) itemView.findViewById(R.id.textTempMax);
            textTempMin = (TextView) itemView.findViewById(R.id.textTempMin);
            textForcaVento = (TextView) itemView.findViewById(R.id.textForcaVento);
            textUmidadeMax = (TextView) itemView.findViewById(R.id.textUmidadeMax);
            textUmidadeMin = (TextView) itemView.findViewById(R.id.textUmidadeMin);
            textResumo = (TextView) itemView.findViewById(R.id.textResumo);

        }
    }

    List<Previsao> listaObj;

    public CustomAdapter(List<Previsao> listaObj, RecyclerView recyclerView) {
        this.listaObj = listaObj;
        this.recyclerView = recyclerView;
    }

    @NonNull
    @Override
    public PrevisaoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_layout, parent, false);
        PrevisaoViewHolder pvh = new PrevisaoViewHolder(v);
        return pvh;
    }

    @Override
    public void onBindViewHolder(PrevisaoViewHolder previsaoViewHolder, int position) {
        if (listaObj.get(position).getIcone() != null && listaObj.get(position).getDiaDaSemana() != null && position < listaObj.size()) {
            previsaoViewHolder.imgPrevisao.setImageBitmap(Bitmap.createScaledBitmap(listaObj.get(position).getIcone(), 320, 320, false));
            previsaoViewHolder.textDia.setText(listaObj.get(position).getData());
            previsaoViewHolder.textDiaSemana.setText(listaObj.get(position).getDiaDaSemana());
            previsaoViewHolder.textTurno.setText(listaObj.get(position).getTurno());
            previsaoViewHolder.textTempMax.setText("Temp. Máx.: " + listaObj.get(position).getTemp_max() + "º");
            previsaoViewHolder.textTempMin.setText("Temp. Min.: " + listaObj.get(position).getTemp_min() + "º");
            previsaoViewHolder.textForcaVento.setText("Força do vento: " + listaObj.get(position).getInt_vento());
            previsaoViewHolder.textUmidadeMax.setText("Umidade Máx.: " + listaObj.get(position).getUmidade_max() + "%");
            previsaoViewHolder.textUmidadeMin.setText("Umidade Min.: " + listaObj.get(position).getUmidade_min() + "%");
            previsaoViewHolder.textResumo.setText(listaObj.get(position).getResumo());
        }
    }

    @Override
    public int getItemCount() {
        return this.listaObj.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }
}
