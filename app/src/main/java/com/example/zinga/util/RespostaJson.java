package com.example.zinga.util;

import org.json.JSONArray;

public interface RespostaJson {

    void atualizaJson(JSONArray array, String url) throws Exception;
}
