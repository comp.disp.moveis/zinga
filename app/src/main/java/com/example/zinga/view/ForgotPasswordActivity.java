package com.example.zinga.view;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.zinga.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;

public class ForgotPasswordActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText forgot_email;
    private Button reset_btn;
    private FirebaseAuth mAuth;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getSupportActionBar().hide();

        forgot_email = (EditText)findViewById(R.id.user_forgot_email);
        reset_btn = (Button)findViewById(R.id.reset_btn);
        reset_btn.setOnClickListener(this);
        mAuth = FirebaseAuth.getInstance();
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.reset_btn){
            PasswordResetEmail(forgot_email.getText().toString());
        }
    }

    /*------------ O código abaixo é para o processo de redefinição de senha, o usuário receberá o e-mail no e-mail registrado -----------*/

    private void PasswordResetEmail(final String email) {
        if(email.equals("")){
            Toast.makeText(ForgotPasswordActivity.this, "Insira o Email!! ", Toast.LENGTH_LONG).show();
        }
        else {
            mAuth.sendPasswordResetEmail(email)
                    .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {
                                Toast.makeText(ForgotPasswordActivity.this, "Enviamos um link de redefinição de senha para o e-mail: " + email, Toast.LENGTH_LONG).show();
                                startActivity(new Intent(ForgotPasswordActivity.this,MainActivity.class));
                            } else {
                                Toast.makeText(ForgotPasswordActivity.this, "Email não encontrado no banco de dados!", Toast.LENGTH_LONG).show();
                            }
                        }
                    });
        }
    }
}
