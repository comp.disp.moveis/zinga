package com.example.zinga.view;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.zinga.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText input_email;
    private EditText input_password;
    private TextView signUp_btn_on_login;
    private TextView forgot_password_on_login;
    private TextView entrando;
    private Button login_btn;
    private FirebaseAuth mAuth;
    private FirebaseUser user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getSupportActionBar().hide();

        input_email = (EditText) findViewById(R.id.input_email);
        input_password = (EditText) findViewById(R.id.input_password);
        signUp_btn_on_login = (TextView) findViewById(R.id.signup_btn_on_login);
        forgot_password_on_login = (TextView) findViewById(R.id.forgot_password_on_login);
        entrando = (TextView) findViewById(R.id.entrando);
        login_btn = (Button) findViewById(R.id.login_btn);

        login_btn.setOnClickListener(this);
        signUp_btn_on_login.setOnClickListener(this);
        forgot_password_on_login.setOnClickListener(this);

        // Inicializar Firebase Auth
        mAuth = FirebaseAuth.getInstance();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        mAuth.signOut();
        finishAffinity();
    }

    @Override
    public void onStart() {
        super.onStart();
        // Verifique se o usuário está conectado (não nulo) e atualize a IU de acordo.
        updateUI(user);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.signup_btn_on_login) {
            startActivity(new Intent(MainActivity.this, SignUpActivity.class));
        } else if (v.getId() == R.id.forgot_password_on_login) {
            startActivity(new Intent(MainActivity.this, ForgotPasswordActivity.class));
        } else if (v.getId() == R.id.login_btn) {
            loginUser(input_email.getText().toString(), input_password.getText().toString());
        }
    }

    /*------------ O código abaixo é para um processo de login bem-sucedido -----------*/

    private void loginUser(String email, String password) {
        if (email.equals("")) {
            Toast.makeText(MainActivity.this, "Insira o Email!!", Toast.LENGTH_SHORT).show();
        } else if (password.equals("")) {
            Toast.makeText(MainActivity.this, "Insira a Senha!!", Toast.LENGTH_SHORT).show();
        } else {
            mAuth.signInWithEmailAndPassword(email, password)
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                // Login bem-sucedido, atualize a IU com as informações do usuário conectado
                                entrando.setVisibility(View.VISIBLE);
                                updateUI(user);
                            } else {
                                // Se o login falhar, exiba uma mensagem para o usuário.
                                Toast.makeText(MainActivity.this, "A autenticação falhou: ", Toast.LENGTH_LONG).show();
                                updateUI(null);
                            }
                        }
                    });
        }
    }

    private void updateUI(FirebaseUser user) {
        user = mAuth.getCurrentUser();
        /*-------- Verifique se o usuário já está logado ou não --------*/
        if (user != null) {
            /*------------ Se o e-mail do usuário for verificado, acesse o login -----------*/
            if (user.isEmailVerified()) {
                Toast.makeText(MainActivity.this, "Login Bem-sucedido.", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(MainActivity.this, ConsultaPrevisaoActivity.class));
            } else {
                Toast.makeText(MainActivity.this, "Seu e-mail não foi verificado.", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
