package com.example.zinga.view;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.zinga.R;
import com.example.zinga.controller.CidadesController;
import com.example.zinga.controller.EstadosController;
import com.example.zinga.controller.PrevisaoController;
import com.example.zinga.controller.UsuarioController;
import com.example.zinga.model.Cidades;
import com.example.zinga.model.Estados;
import com.example.zinga.model.Previsao;
import com.example.zinga.model.Usuario;
import com.example.zinga.util.CustomAdapter;
import com.example.zinga.util.HttpService;
import com.example.zinga.util.RespostaJson;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import org.json.JSONArray;

import java.util.List;

public class ConsultaPrevisaoActivity extends AppCompatActivity implements RespostaJson {

    private final String urlEstado = "https://servicodados.ibge.gov.br/api/v1/localidades/estados/";
    private final String urlCidades = "https://servicodados.ibge.gov.br/api/v1/localidades/estados/{UF}/distritos";
    private final String urlPrevisao = "https://apiprevmet3.inmet.gov.br/previsao/{geocode}";
    private Spinner spinnerEstados;
    private Spinner spinnerCidades;
    private CidadesController cController;
    private EstadosController eController;
    private PrevisaoController pController;
    private RecyclerView recyclerView;
    private Object cidadeSelecionada;
    private ProgressDialog progress;
    private int geocode;
    private ImageView user_profile;
    FirebaseAuth mAuth;
    FirebaseUser user;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_consulta_previsao);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getSupportActionBar().hide();
        progress = new ProgressDialog(this);
        progress.setMessage("Loading...");
        user_profile = (ImageView) findViewById(R.id.user_profile);

        cController = new CidadesController();
        eController = new EstadosController(getApplicationContext());

        mAuth = FirebaseAuth.getInstance();
        user = mAuth.getCurrentUser();

        getHttpService(urlEstado);

        if (user != null) {
            try {
                UsuarioController uController = new UsuarioController();
                Usuario usuario = uController.findUser(user.getEmail(), getApplicationContext());
                System.out.println("<-- FOTO --> " + usuario.getFoto());
                user_profile.setImageBitmap(uController.BlobToBitmap(usuario));
                System.out.println("<-- USUARIO --> " + usuario.toString());
            } catch (Exception e) {
                Log.e("ERRO", e.getMessage());
            }
        }
    }

    public void redefinirSenha(View view) {
        startActivity(new Intent(ConsultaPrevisaoActivity.this, DashboardActivity.class));
    }

    public void sair(View view) {
        mAuth.signOut();
        startActivity(new Intent(ConsultaPrevisaoActivity.this, MainActivity.class));
        finish();
    }

    public void getHttpService(String url) {
        HttpService httpService = new HttpService(this, progress, url);
        httpService.execute(url);
    }

    public void spinnerEstados(List<Estados> listEstados) {
        spinnerEstados = (Spinner) findViewById(R.id.spinnerEstados);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.simple_spinner_item) {
            @Override
            public boolean isEnabled(int position) {
                return position != 0;
            }

            @Override
            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;

                if (position == 0) {
                    tv.setTextColor(Color.GRAY);

                } else {
                    tv.setTextColor(Color.BLACK);
                }
                return view;
            }
        };
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        adapter.add("Escolha o Estado");
        for (Estados estados : listEstados) {
            adapter.add(estados.getNome());
        }
        spinnerEstados.setAdapter(adapter);

        spinnerEstados.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Object estadoSelecionado = parent.getItemAtPosition(position);
                if (estadoSelecionado != null && !estadoSelecionado.equals("Escolha o Estado")) {
                    int uf = eController.getUfEstado(listEstados, estadoSelecionado);
                    String url = urlCidades.replace("{UF}", Integer.toString(uf));
                    getHttpService(url);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        if (progress.isShowing()) {
            progress.dismiss();
        }
    }

    public void spinnerCidades(List<Cidades> list) {
        spinnerCidades = (Spinner) findViewById(R.id.spinnerCidades);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.simple_spinner_item) {
            @Override
            public boolean isEnabled(int position) {
                return position != 0;
            }

            @Override
            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;

                if (position == 0) {
                    tv.setTextColor(Color.GRAY);

                } else {
                    tv.setTextColor(Color.BLACK);
                }
                return view;
            }
        };
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        adapter.add("Escolha a Cidade");
        for (Cidades cidades : list) {
            adapter.add(cidades.getNome());
        }
        spinnerCidades.setAdapter(adapter);
        spinnerCidades.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                cidadeSelecionada = parent.getItemAtPosition(position);
                if (cidadeSelecionada != null && !cidadeSelecionada.equals("Escolha a Cidade")) {
                    geocode = cController.getGeocodeCidade(list, cidadeSelecionada);
                    String url = urlPrevisao.replace("{geocode}", Integer.toString(geocode));
                    getHttpService(url);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        if (progress.isShowing()) {
            progress.dismiss();
        }
    }

    public void customAdapter(List<Previsao> list) {
        TextView textView = findViewById(R.id.textTitulo);
        textView.setText(cidadeSelecionada.toString());
        recyclerView = (RecyclerView) findViewById(R.id.showRecycler);
        CustomAdapter customAdapter = new CustomAdapter(list, recyclerView);
        RecyclerView.LayoutManager layout = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
//        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 3);
        recyclerView.setAdapter(customAdapter);
        recyclerView.setLayoutManager(layout);
        if (progress.isShowing()) {
            progress.dismiss();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void atualizaJson(JSONArray array, String url) throws Exception {
        if (url.contains("previsao")) {
            pController = new PrevisaoController();
            customAdapter(pController.getJsonIterable(array));
        } else if (url.contains("distritos")) {
            spinnerCidades(cController.getJsonIterable(array));
        } else if (url.equals(urlEstado)) {
            spinnerEstados(eController.getJsonIterable(array));
        }
    }
}