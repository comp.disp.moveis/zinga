package com.example.zinga.view;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.zinga.R;
import com.example.zinga.controller.UsuarioController;
import com.example.zinga.model.Usuario;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class DashboardActivity extends AppCompatActivity implements View.OnClickListener {

    private Button voltar;
    private Button change_password;
    private Button update_password;
    private EditText new_password;
    private TextView welcome_text;
    private ImageView user_profile;
    private CountDownTimer timer;
    private boolean session_out = false;
    private FirebaseAuth mAuth;
    private FirebaseUser user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getSupportActionBar().hide();

        welcome_text = (TextView) findViewById(R.id.welcome_text);
        voltar = (Button) findViewById(R.id.voltar);
        change_password = (Button) findViewById(R.id.change_password);
        update_password = (Button) findViewById(R.id.update_password);
        new_password = (EditText) findViewById(R.id.new_password);
        user_profile = (ImageView) findViewById(R.id.user_profile);

        voltar.setOnClickListener(this);
        change_password.setOnClickListener(this);
        update_password.setOnClickListener(this);

        mAuth = FirebaseAuth.getInstance();
        user = mAuth.getCurrentUser();
        if (user != null) {
            String name = user.getDisplayName();
            String email_id = user.getEmail();
            try {
                UsuarioController uController = new UsuarioController();
                Usuario usuario = uController.findUser(user.getEmail(), getApplicationContext());
                user_profile.setImageBitmap(uController.BlobToBitmap(usuario));
//                System.out.println("--> " + image_uri.toString());
            } catch (Exception e) {
                Log.e("ERRO", e.getMessage());
            }
            welcome_text.setText("Bem vindo " + name + "\n" + email_id);
        }

        /*------------ O código abaixo é para logout automático na inatividade do usuário -----------*/
        timer = new CountDownTimer(300000, 1000) { //set session timeout interval here
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                session_out = true;
            }
        };

    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.voltar) {
            startActivity(new Intent(DashboardActivity.this, ConsultaPrevisaoActivity.class));
        } else if (v.getId() == R.id.change_password) {
            new_password.setVisibility(View.VISIBLE);
            update_password.setVisibility(View.VISIBLE);

        } else if (v.getId() == R.id.update_password) {
            ChangePasswordRequest(new_password.getText().toString());
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        //Iniciar cronômetro em inatividade
        Log.i("Main", "Cronômetro iniciado!");
        timer.start();

    }

    @Override
    protected void onResume() {
        super.onResume();
        //se o usuário fizer alguma atividade, cancele o cronômetro
        if (timer != null) {
            Log.i("Main", "Cronômetro cancelado!");
            timer.cancel();
        }
        //Se o usuário voltar após o tempo limite da sessão, redirecionar para a página de login
        if (session_out == true) {
            mAuth.signOut();
            Toast.makeText(DashboardActivity.this, "Sessão expirada!!.", Toast.LENGTH_LONG).show();
            startActivity(new Intent(DashboardActivity.this, MainActivity.class));
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        //Ao fechar o aplicativo de usuário desconectado
        mAuth.signOut();
    }

    /*------------ O código abaixo é para alterar o processo de senha -----------*/

    private void ChangePasswordRequest(String new_password) {
        if (new_password.equals("")) {
            Toast.makeText(DashboardActivity.this, "Insira a Senha!! ", Toast.LENGTH_LONG).show();
        } else {
            FirebaseUser user = mAuth.getCurrentUser();
            user.updatePassword(new_password)
                    .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {
                                Toast.makeText(DashboardActivity.this, "Senha atualizada.", Toast.LENGTH_LONG).show();
                            } else {
                                Toast.makeText(DashboardActivity.this, "Por motivos de segurança, você deve primeiro fazer o login novamente.\n" +
                                        "Então tente atualizar a senha.", Toast.LENGTH_LONG).show();
                            }
                        }
                    });
        }

    }
}
