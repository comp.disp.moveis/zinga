package com.example.zinga.view;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;

import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.zinga.R;
import com.example.zinga.controller.UsuarioController;
import com.example.zinga.model.Usuario;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;

public class SignUpActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText user_email;
    private EditText user_password;
    private EditText user_name;
    private TextView login_btn_on_signup;
    private TextView carregando;
    private Button signup_btn;
    private ImageView user_profile;
    private Uri image_uri;
    private FirebaseAuth mAuth;
    private FirebaseUser user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getSupportActionBar().hide();

        carregando = (TextView) findViewById(R.id.carregando);
        login_btn_on_signup = (TextView) findViewById(R.id.login_btn_on_signup);
        user_email = (EditText) findViewById(R.id.user_email);
        user_password = (EditText) findViewById(R.id.user_password);
        user_name = (EditText) findViewById(R.id.user_name);
        user_profile = (ImageView) findViewById(R.id.add_pic);
        signup_btn = (Button) findViewById(R.id.register_btn);

        login_btn_on_signup.setOnClickListener(this);
        signup_btn.setOnClickListener(this);
        user_profile.setOnClickListener(this);

        // Initialize Firebase Auth
        mAuth = FirebaseAuth.getInstance();
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.login_btn_on_signup) {
            startActivity(new Intent(SignUpActivity.this, MainActivity.class));
        } else if (v.getId() == R.id.register_btn) {
            signUpUser(user_email.getText().toString(), user_password.getText().toString());
            carregando.setVisibility(View.VISIBLE);
        } else if (v.getId() == R.id.add_pic) {
            SelectProfilePic();
        }
    }

    /*-------- O código abaixo é para selecionar uma imagem da galeria ou câmera -----------*/

    private void SelectProfilePic() {
        final CharSequence[] options = {"Tirar foto", "Escolha na galeria", "Cancelar"};
        AlertDialog.Builder builder = new AlertDialog.Builder(SignUpActivity.this);
        builder.setTitle("Adicionar foto!");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("Tirar foto")) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (checkSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_DENIED ||
                                checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED ||
                                checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED) {
                            String[] permission = {Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE};
                            requestPermissions(permission, 1000);
                        } else {
                            openCamera();
                        }
                    } else {
                        openCamera();
                    }
                } else if (options[item].equals("Escolha na galeria")) {

                    Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
                    startActivityForResult(intent, 2);

                } else if (options[item].equals("Cancelar")) {

                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void openCamera() {
        ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.TITLE, "Nova foto");
        values.put(MediaStore.Images.Media.DESCRIPTION, "From Camera");
        image_uri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);

        //Camera intent
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, image_uri);
        startActivityForResult(takePictureIntent, 1);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 1000:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    openCamera();
                } else {
                    //Permissão de pop up foi negada.
                    Toast.makeText(SignUpActivity.this, "Permissão negada...", Toast.LENGTH_LONG).show();
                }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        // O código de resultado é RESULT_OK apenas se o usuário selecionar uma imagem
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case 1:
                    user_profile.setImageURI(image_uri);
                    break;
                case 2:
                    //data.getData retorna o URI de conteúdo para a imagem selecionada
                    image_uri = data.getData();
                    user_profile.setImageURI(image_uri);
                    break;
            }
        }
    }

    /*------------ O código abaixo é para um processo de inscrição bem-sucedido -----------*/

    private void signUpUser(String email, String password) {
        if (email.equals("")) {
            Toast.makeText(SignUpActivity.this, "Insira o Email!!",
                    Toast.LENGTH_SHORT).show();
        } else if (password.equals("")) {
            Toast.makeText(SignUpActivity.this, "Insira a Senha!!",
                    Toast.LENGTH_SHORT).show();
        } else if ((user_name.getText().toString()).equals("")) {
            Toast.makeText(SignUpActivity.this, "Insira o Nome!!",
                    Toast.LENGTH_SHORT).show();
        } else if (image_uri == null) {
            Toast.makeText(SignUpActivity.this, "Selecione o Perfil!!",
                    Toast.LENGTH_SHORT).show();
        } else {
            mAuth.createUserWithEmailAndPassword(email, password)
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                // Login bem-sucedido, atualize a IU com as informações do usuário conectado.
                                userProfile();
                            } else {
                                // Se o login falhar, exibe uma mensagem para o usuário.
                                Toast.makeText(SignUpActivity.this, "Falha ao Entrar ", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
        }
    }

    /*----------Para salvar a imagem e o nome do usuário no Firebase Database-------*/
    private void userProfile() {
        user = mAuth.getCurrentUser();
        if (user != null) {
            UsuarioController uController = new UsuarioController();

            byte[] imgBase64 = uController.uriToBlob(image_uri, this.getContentResolver());

            Usuario usuario = new Usuario();
            usuario.setNome(user_name.getText().toString());
            usuario.setEmail(user_email.getText().toString());
            usuario.setFoto(imgBase64);

            if (uController.insertUser(usuario, getApplicationContext())) {
                Toast.makeText(SignUpActivity.this, "Usuário adicionado com sucesso!!", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(SignUpActivity.this, "Erro ao adicionar usuário!!", Toast.LENGTH_SHORT).show();
            }

            UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                    .setDisplayName(user_name.getText().toString())
                    .setPhotoUri(image_uri).build();
            user.updateProfile(profileUpdates).addOnCompleteListener(this, new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    verifyEmailRequest();
                }
            });
        }
    }

    /*-------Para enviar e-mail de verificação no e-mail registrado do usuário------*/
    private void verifyEmailRequest() {
        user.sendEmailVerification()
                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            Toast.makeText(SignUpActivity.this, "Verificação de e-mail enviada em\n" + user_email.getText().toString(), Toast.LENGTH_LONG).show();
                            mAuth.signOut();
                            startActivity(new Intent(SignUpActivity.this, MainActivity.class));
                        } else {
                            Toast.makeText(SignUpActivity.this, "Sucesso na inscrição, mas falha ao enviar e-mail de verificação.", Toast.LENGTH_LONG).show();
                        }
                    }
                });
    }
}
