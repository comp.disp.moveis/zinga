package com.example.zinga.model;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class Cidades {

    private int id;
    private int geoCode;
    private String nome;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setGeoCode(int geoCode) {
        this.geoCode = geoCode;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getGeoCode() {
        return geoCode;
    }

    public String getNome() {
        return nome;
    }

    @Override
    public String toString() {
        return "id: " + getId()
                + ", geocode: " + getGeoCode()
                + ", nome: " + getNome();
    }

    public List<Cidades> jsonIterable(JSONArray jsonArray) throws Exception {
        List<Cidades> listCidades = new ArrayList<>();
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject obj = jsonArray.getJSONObject(i);
            JSONObject objMunicipio = obj.getJSONObject("municipio");

            Cidades c = new Cidades();
            c.setGeoCode(objMunicipio.getInt("id"));
            c.setNome(objMunicipio.getString("nome"));

            listCidades.add(c);
        }
        return listCidades;
    }
}
