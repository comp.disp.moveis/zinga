package com.example.zinga.model;

public class Usuario {

    private int id;
    private String nome;
    private String email;
    private byte[] foto;

    public long getId() {
        return id;
    }

    public String getNome() {
        return nome;
    }

    public String getEmail() {
        return email;
    }

    public byte[] getFoto() {
        return foto;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setFoto(byte[] foto) {
        this.foto = foto;
    }

    @Override
    public String toString() {
        return "\nId: " + id +
                "\nNome: " + nome +
                "\nEmail: " + email +
                "\nFoto: " + foto;
    }
}
