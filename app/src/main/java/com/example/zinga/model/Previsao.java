package com.example.zinga.model;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Previsao {

    private Bitmap icone;
    private Bitmap temp_max_tende_icone;
    private Bitmap temp_min_tende_icone;
    private String turno;
    private String data;
    private String resumo;
    private String int_vento;
    private String temp_max_tende;
    private String temp_min_tende;
    private String diaDaSemana;
    private int umidade_max;
    private int umidade_min;
    private int temp_max;
    private int temp_min;

    public String getTurno() {
        return turno;
    }

    public void setTurno(String turno) {
        this.turno = turno;
    }

    public String getResumo() {
        return resumo;
    }

    public void setResumo(String resumo) {
        this.resumo = resumo;
    }

    public String getInt_vento() {
        return int_vento;
    }

    public void setInt_vento(String int_vento) {
        this.int_vento = int_vento;
    }

    public Bitmap getIcone() {
        return icone;
    }

    public void setIcone(String icone) {
        this.icone = stringToImage(icone);
    }

    public int getUmidade_max() {
        return umidade_max;
    }

    public void setUmidade_max(int umidade_max) {
        this.umidade_max = umidade_max;
    }

    public int getUmidade_min() {
        return umidade_min;
    }

    public void setUmidade_min(int umidade_min) {
        this.umidade_min = umidade_min;
    }

    public String getTemp_max_tende() {
        return temp_max_tende;
    }

    public void setTemp_max_tende(String temp_max_tende) {
        this.temp_max_tende = temp_max_tende;
    }

    public String getTemp_min_tende() {
        return temp_min_tende;
    }

    public void setTemp_min_tende(String temp_min_tende) {
        this.temp_min_tende = temp_min_tende;
    }

    public String getDiaDaSemana() {
        return diaDaSemana;
    }

    public void setDiaDaSemana(String diaDaSemana) {
        this.diaDaSemana = diaDaSemana;
    }

    public Bitmap getTemp_max_tende_icone() {
        return temp_max_tende_icone;
    }

    public void setTemp_max_tende_icone(String temp_max_tende_icone) {
        this.temp_max_tende_icone = stringToImage(temp_max_tende_icone);
    }

    public Bitmap getTemp_min_tende_icone() {
        return temp_min_tende_icone;
    }

    public void setTemp_min_tende_icone(String temp_min_tende_icone) {
        this.temp_min_tende_icone = stringToImage(temp_min_tende_icone);
    }

    public int getTemp_max() {
        return temp_max;
    }

    public void setTemp_max(int temp_max) {
        this.temp_max = temp_max;
    }

    public int getTemp_min() {
        return temp_min;
    }

    public void setTemp_min(int temp_min) {
        this.temp_min = temp_min;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public Bitmap stringToImage(String imagem) {
        String imageDataBytes = imagem.replace("data:image/png;base64,", "");
        byte[] decodedString = Base64.decode(imageDataBytes, Base64.DEFAULT);
        InputStream is = new ByteArrayInputStream(decodedString);

        Bitmap decodeByte = BitmapFactory.decodeStream(is, null, null);
        return decodeByte;
    }

    public Date stringToDate(String dataPrevisao) throws ParseException {
        SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
        return formato.parse(dataPrevisao);
    }

    public String dateToString(Date data) {
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        return dateFormat.format(data);
    }

    public String[] dataCincoDias() {
        String[] datas = new String[5];

        for (int i = 0; i < datas.length; i++) {
            Date data = new Date();
            data.setDate(data.getDate() + i);
            datas[i] = dateToString(data);
        }
        return datas;
    }

    @Override
    public String toString() {
        return "\nPrevisao {" +
                "\ndata= " + data +
                "\n" + '}';
    }

    public class Manha extends Previsao {
        private Bitmap icone;
        private Bitmap temp_max_tende_icone;
        private Bitmap temp_min_tende_icone;
        private String turno;
        private String data;
        private String resumo;
        private String int_vento;
        private String temp_max_tende;
        private String temp_min_tende;
        private String diaDaSemana;
        private int umidade_max;
        private int umidade_min;
        private int temp_max;
        private int temp_min;

        @Override
        public String getTurno() {
            return turno;
        }

        @Override
        public void setTurno(String turno) {
            this.turno = turno;
        }

        @Override
        public String getData() {
            return data;
        }

        @Override
        public void setData(String data) {
            this.data = data;
        }

        @Override
        public String getResumo() {
            return resumo;
        }

        @Override
        public void setResumo(String resumo) {
            this.resumo = resumo;
        }

        @Override
        public String getInt_vento() {
            return int_vento;
        }

        @Override
        public void setInt_vento(String int_vento) {
            this.int_vento = int_vento;
        }

        @Override
        public Bitmap getIcone() {
            return icone;
        }

        @Override
        public void setIcone(String icone) {
            this.icone = stringToImage(icone);
        }

        @Override
        public int getUmidade_max() {
            return umidade_max;
        }

        @Override
        public void setUmidade_max(int umidade_max) {
            this.umidade_max = umidade_max;
        }

        @Override
        public int getUmidade_min() {
            return umidade_min;
        }

        @Override
        public void setUmidade_min(int umidade_min) {
            this.umidade_min = umidade_min;
        }

        @Override
        public String getTemp_max_tende() {
            return temp_max_tende;
        }

        @Override
        public void setTemp_max_tende(String temp_max_tende) {
            this.temp_max_tende = temp_max_tende;
        }

        @Override
        public String getTemp_min_tende() {
            return temp_min_tende;
        }

        @Override
        public void setTemp_min_tende(String temp_min_tende) {
            this.temp_min_tende = temp_min_tende;
        }

        @Override
        public String getDiaDaSemana() {
            return diaDaSemana;
        }

        @Override
        public void setDiaDaSemana(String diaDaSemana) {
            this.diaDaSemana = diaDaSemana;
        }

        @Override
        public Bitmap getTemp_max_tende_icone() {
            return temp_max_tende_icone;
        }

        @Override
        public void setTemp_max_tende_icone(String temp_max_tende_icone) {
            this.temp_max_tende_icone = stringToImage(temp_max_tende_icone);
        }

        @Override
        public Bitmap getTemp_min_tende_icone() {
            return temp_min_tende_icone;
        }

        @Override
        public void setTemp_min_tende_icone(String temp_min_tende_icone) {
            this.temp_min_tende_icone = stringToImage(temp_min_tende_icone);
        }

        @Override
        public int getTemp_max() {
            return temp_max;
        }

        @Override
        public void setTemp_max(int temp_max) {
            this.temp_max = temp_max;
        }

        @Override
        public int getTemp_min() {
            return temp_min;
        }

        @Override
        public void setTemp_min(int temp_min) {
            this.temp_min = temp_min;
        }

        @Override
        public String toString() {
            return "\nManha { " +
                    "\ndata= " + data +
                    "\nicone= " + icone +
                    "\ntemp_max_tende_icone= " + temp_max_tende_icone +
                    "\ntemp_min_tende_icone= " + temp_min_tende_icone +
                    "\nresumo= " + resumo +
                    "\nint_vento= " + int_vento +
                    "\ntemp_max_tende= " + temp_max_tende +
                    "\ntemp_min_tende= " + temp_min_tende +
                    "\ndiaDaSemana= " + diaDaSemana +
                    "\numidade_max= " + umidade_max +
                    "\numidade_min= " + umidade_min +
                    "\ntemp_max= " + temp_max +
                    "\ntemp_min= " + temp_min +
                    "\n" + '}';
        }
    }

    public class Tarde extends Previsao {
        private Bitmap icone;
        private Bitmap temp_max_tende_icone;
        private Bitmap temp_min_tende_icone;
        private String turno;
        private String data;
        private String resumo;
        private String int_vento;
        private String temp_max_tende;
        private String temp_min_tende;
        private String diaDaSemana;
        private int umidade_max;
        private int umidade_min;
        private int temp_max;
        private int temp_min;

        @Override
        public String getTurno() {
            return turno;
        }

        @Override
        public void setTurno(String turno) {
            this.turno = turno;
        }

        @Override
        public String getData() {
            return data;
        }

        @Override
        public void setData(String data) {
            this.data = data;
        }

        @Override
        public String getResumo() {
            return resumo;
        }

        @Override
        public void setResumo(String resumo) {
            this.resumo = resumo;
        }

        @Override
        public String getInt_vento() {
            return int_vento;
        }

        @Override
        public void setInt_vento(String int_vento) {
            this.int_vento = int_vento;
        }

        @Override
        public Bitmap getIcone() {
            return icone;
        }

        @Override
        public void setIcone(String icone) {
            this.icone = stringToImage(icone);
        }

        @Override
        public int getUmidade_max() {
            return umidade_max;
        }

        @Override
        public void setUmidade_max(int umidade_max) {
            this.umidade_max = umidade_max;
        }

        @Override
        public int getUmidade_min() {
            return umidade_min;
        }

        @Override
        public void setUmidade_min(int umidade_min) {
            this.umidade_min = umidade_min;
        }

        @Override
        public String getTemp_max_tende() {
            return temp_max_tende;
        }

        @Override
        public void setTemp_max_tende(String temp_max_tende) {
            this.temp_max_tende = temp_max_tende;
        }

        @Override
        public String getTemp_min_tende() {
            return temp_min_tende;
        }

        @Override
        public void setTemp_min_tende(String temp_min_tende) {
            this.temp_min_tende = temp_min_tende;
        }

        @Override
        public String getDiaDaSemana() {
            return diaDaSemana;
        }

        @Override
        public void setDiaDaSemana(String diaDaSemana) {
            this.diaDaSemana = diaDaSemana;
        }

        @Override
        public Bitmap getTemp_max_tende_icone() {
            return temp_max_tende_icone;
        }

        @Override
        public void setTemp_max_tende_icone(String temp_max_tende_icone) {
            this.temp_max_tende_icone = stringToImage(temp_max_tende_icone);
        }

        @Override
        public Bitmap getTemp_min_tende_icone() {
            return temp_min_tende_icone;
        }

        @Override
        public void setTemp_min_tende_icone(String temp_min_tende_icone) {
            this.temp_min_tende_icone = stringToImage(temp_min_tende_icone);
        }

        @Override
        public int getTemp_max() {
            return temp_max;
        }

        @Override
        public void setTemp_max(int temp_max) {
            this.temp_max = temp_max;
        }

        @Override
        public int getTemp_min() {
            return temp_min;
        }

        @Override
        public void setTemp_min(int temp_min) {
            this.temp_min = temp_min;
        }

        @Override
        public String toString() {
            return "\nTarde { " +
                    "\ndata= " + data +
                    "\nicone= " + icone +
                    "\ntemp_max_tende_icone= " + temp_max_tende_icone +
                    "\ntemp_min_tende_icone= " + temp_min_tende_icone +
                    "\nresumo= " + resumo +
                    "\nint_vento= " + int_vento +
                    "\ntemp_max_tende= " + temp_max_tende +
                    "\ntemp_min_tende= " + temp_min_tende +
                    "\ndiaDaSemana= " + diaDaSemana +
                    "\numidade_max= " + umidade_max +
                    "\numidade_min= " + umidade_min +
                    "\ntemp_max= " + temp_max +
                    "\ntemp_min= " + temp_min +
                    "\n" + '}';
        }
    }

    public class Noite extends Previsao {
        private Bitmap icone;
        private Bitmap temp_max_tende_icone;
        private Bitmap temp_min_tende_icone;
        private String turno;
        private String data;
        private String resumo;
        private String int_vento;
        private String temp_max_tende;
        private String temp_min_tende;
        private String diaDaSemana;
        private int umidade_max;
        private int umidade_min;
        private int temp_max;
        private int temp_min;

        @Override
        public String getTurno() {
            return turno;
        }

        @Override
        public void setTurno(String turno) {
            this.turno = turno;
        }

        @Override
        public String getData() {
            return data;
        }

        @Override
        public void setData(String data) {
            this.data = data;
        }

        @Override
        public String getResumo() {
            return resumo;
        }

        @Override
        public void setResumo(String resumo) {
            this.resumo = resumo;
        }

        @Override
        public String getInt_vento() {
            return int_vento;
        }

        @Override
        public void setInt_vento(String int_vento) {
            this.int_vento = int_vento;
        }

        @Override
        public Bitmap getIcone() {
            return icone;
        }

        @Override
        public void setIcone(String icone) {
            this.icone = stringToImage(icone);
        }

        @Override
        public int getUmidade_max() {
            return umidade_max;
        }

        @Override
        public void setUmidade_max(int umidade_max) {
            this.umidade_max = umidade_max;
        }

        @Override
        public int getUmidade_min() {
            return umidade_min;
        }

        @Override
        public void setUmidade_min(int umidade_min) {
            this.umidade_min = umidade_min;
        }

        @Override
        public String getTemp_max_tende() {
            return temp_max_tende;
        }

        @Override
        public void setTemp_max_tende(String temp_max_tende) {
            this.temp_max_tende = temp_max_tende;
        }

        @Override
        public String getTemp_min_tende() {
            return temp_min_tende;
        }

        @Override
        public void setTemp_min_tende(String temp_min_tende) {
            this.temp_min_tende = temp_min_tende;
        }

        @Override
        public String getDiaDaSemana() {
            return diaDaSemana;
        }

        @Override
        public void setDiaDaSemana(String diaDaSemana) {
            this.diaDaSemana = diaDaSemana;
        }

        @Override
        public Bitmap getTemp_max_tende_icone() {
            return temp_max_tende_icone;
        }

        @Override
        public void setTemp_max_tende_icone(String temp_max_tende_icone) {
            this.temp_max_tende_icone = stringToImage(temp_max_tende_icone);
        }

        @Override
        public Bitmap getTemp_min_tende_icone() {
            return temp_min_tende_icone;
        }

        @Override
        public void setTemp_min_tende_icone(String temp_min_tende_icone) {
            this.temp_min_tende_icone = stringToImage(temp_min_tende_icone);
        }

        @Override
        public int getTemp_max() {
            return temp_max;
        }

        @Override
        public void setTemp_max(int temp_max) {
            this.temp_max = temp_max;
        }

        @Override
        public int getTemp_min() {
            return temp_min;
        }

        @Override
        public void setTemp_min(int temp_min) {
            this.temp_min = temp_min;
        }

        @Override
        public String toString() {
            return "\nNoite { " +
                    "\ndata= " + data +
                    "\nicone= " + icone +
                    "\ntemp_max_tende_icone= " + temp_max_tende_icone +
                    "\ntemp_min_tende_icone= " + temp_min_tende_icone +
                    "\nresumo= " + resumo +
                    "\nint_vento= " + int_vento +
                    "\ntemp_max_tende= " + temp_max_tende +
                    "\ntemp_min_tende= " + temp_min_tende +
                    "\ndiaDaSemana= " + diaDaSemana +
                    "\numidade_max= " + umidade_max +
                    "\numidade_min= " + umidade_min +
                    "\ntemp_max= " + temp_max +
                    "\ntemp_min= " + temp_min +
                    "\n" + '}';
        }
    }

    public class UltimosDias extends Previsao {
        private Bitmap icone;
        private Bitmap temp_max_tende_icone;
        private Bitmap temp_min_tende_icone;
        private String turno;
        private String data;
        private String resumo;
        private String int_vento;
        private String temp_max_tende;
        private String temp_min_tende;
        private String diaDaSemana;
        private int umidade_max;
        private int umidade_min;
        private int temp_max;
        private int temp_min;

        @Override
        public String getTurno() {
            return turno;
        }

        @Override
        public void setTurno(String turno) {
            this.turno = turno;
        }

        @Override
        public String getData() {
            return data;
        }

        @Override
        public void setData(String data) {
            this.data = data;
        }

        @Override
        public String getResumo() {
            return resumo;
        }

        @Override
        public void setResumo(String resumo) {
            this.resumo = resumo;
        }

        @Override
        public String getInt_vento() {
            return int_vento;
        }

        @Override
        public void setInt_vento(String int_vento) {
            this.int_vento = int_vento;
        }

        @Override
        public Bitmap getIcone() {
            return icone;
        }

        @Override
        public void setIcone(String icone) {
            this.icone = stringToImage(icone);
        }

        @Override
        public int getUmidade_max() {
            return umidade_max;
        }

        @Override
        public void setUmidade_max(int umidade_max) {
            this.umidade_max = umidade_max;
        }

        @Override
        public int getUmidade_min() {
            return umidade_min;
        }

        @Override
        public void setUmidade_min(int umidade_min) {
            this.umidade_min = umidade_min;
        }

        @Override
        public String getTemp_max_tende() {
            return temp_max_tende;
        }

        @Override
        public void setTemp_max_tende(String temp_max_tende) {
            this.temp_max_tende = temp_max_tende;
        }

        @Override
        public String getTemp_min_tende() {
            return temp_min_tende;
        }

        @Override
        public void setTemp_min_tende(String temp_min_tende) {
            this.temp_min_tende = temp_min_tende;
        }

        @Override
        public String getDiaDaSemana() {
            return diaDaSemana;
        }

        @Override
        public void setDiaDaSemana(String diaDaSemana) {
            this.diaDaSemana = diaDaSemana;
        }

        @Override
        public Bitmap getTemp_max_tende_icone() {
            return temp_max_tende_icone;
        }

        @Override
        public void setTemp_max_tende_icone(String temp_max_tende_icone) {
            this.temp_max_tende_icone = stringToImage(temp_max_tende_icone);
        }

        @Override
        public Bitmap getTemp_min_tende_icone() {
            return temp_min_tende_icone;
        }

        @Override
        public void setTemp_min_tende_icone(String temp_min_tende_icone) {
            this.temp_min_tende_icone = stringToImage(temp_min_tende_icone);
        }

        @Override
        public int getTemp_max() {
            return temp_max;
        }

        @Override
        public void setTemp_max(int temp_max) {
            this.temp_max = temp_max;
        }

        @Override
        public int getTemp_min() {
            return temp_min;
        }

        @Override
        public void setTemp_min(int temp_min) {
            this.temp_min = temp_min;
        }

        @Override
        public String toString() {
            return "\nUltimos dias { " +
                    "\ndata= " + data +
                    "\nicone= " + icone +
                    "\ntemp_max_tende_icone= " + temp_max_tende_icone +
                    "\ntemp_min_tende_icone= " + temp_min_tende_icone +
                    "\nresumo= " + resumo +
                    "\nint_vento= " + int_vento +
                    "\ntemp_max_tende= " + temp_max_tende +
                    "\ntemp_min_tende= " + temp_min_tende +
                    "\ndiaDaSemana= " + diaDaSemana +
                    "\numidade_max= " + umidade_max +
                    "\numidade_min= " + umidade_min +
                    "\ntemp_max= " + temp_max +
                    "\ntemp_min= " + temp_min +
                    "\n" + '}';
        }
    }

    public List<Previsao> jsonIterable(JSONArray jsonArray) throws Exception {
        String[] datas = dataCincoDias();
        List<Previsao> listPrevisao = new ArrayList<>();
        JSONObject obj = jsonArray.getJSONObject(0);

        for (int i = 0; i < obj.length(); i++) {
            JSONObject objPrevisao = jsonArray.getJSONObject(0);
            JSONObject objData = objPrevisao.getJSONObject(datas[i]);

            if (i <= 1) {
                JSONObject objManha = objData.getJSONObject("manha");
                Previsao.Manha manha = new Manha();
                manha.setData(datas[i]);
                manha.setTurno("Manhã");
                manha.setResumo(objManha.getString("resumo"));
                manha.setTemp_max(objManha.getInt("temp_max"));
                manha.setTemp_min(objManha.getInt("temp_min"));
                manha.setInt_vento(objManha.getString("int_vento"));
                manha.setIcone(objManha.getString("icone"));
                manha.setDiaDaSemana(objManha.getString("dia_semana"));
                manha.setUmidade_max(objManha.getInt("umidade_max"));
                manha.setUmidade_min(objManha.getInt("umidade_min"));
                manha.setTemp_max_tende(objManha.getString("temp_max_tende"));
                manha.setTemp_max_tende_icone(objManha.getString("temp_max_tende_icone"));
                manha.setTemp_min_tende(objManha.getString("temp_min_tende"));
                manha.setTemp_min_tende_icone(objManha.getString("temp_min_tende_icone"));

                JSONObject objTarde = objData.getJSONObject("tarde");
                Previsao.Tarde tarde = new Tarde();
                tarde.setData(datas[i]);
                tarde.setTurno("Tarde");
                tarde.setResumo(objTarde.getString("resumo"));
                tarde.setTemp_max(objTarde.getInt("temp_max"));
                tarde.setTemp_min(objTarde.getInt("temp_min"));
                tarde.setInt_vento(objTarde.getString("int_vento"));
                tarde.setIcone(objTarde.getString("icone"));
                tarde.setDiaDaSemana(objTarde.getString("dia_semana"));
                tarde.setUmidade_max(objTarde.getInt("umidade_max"));
                tarde.setUmidade_min(objTarde.getInt("umidade_min"));
                tarde.setTemp_max_tende(objTarde.getString("temp_max_tende"));
                tarde.setTemp_max_tende_icone(objTarde.getString("temp_max_tende_icone"));
                tarde.setTemp_min_tende(objTarde.getString("temp_min_tende"));
                tarde.setTemp_min_tende_icone(objTarde.getString("temp_min_tende_icone"));

                JSONObject objNoite = objData.getJSONObject("noite");
                Previsao.Noite noite = new Noite();
                noite.setData(datas[i]);
                noite.setTurno("Noite");
                noite.setResumo(objNoite.getString("resumo"));
                noite.setTemp_max(objNoite.getInt("temp_max"));
                noite.setTemp_min(objNoite.getInt("temp_min"));
                noite.setInt_vento(objNoite.getString("int_vento"));
                noite.setIcone(objNoite.getString("icone"));
                noite.setDiaDaSemana(objNoite.getString("dia_semana"));
                noite.setUmidade_max(objNoite.getInt("umidade_max"));
                noite.setUmidade_min(objNoite.getInt("umidade_min"));
                noite.setTemp_max_tende(objNoite.getString("temp_max_tende"));
                noite.setTemp_max_tende_icone(objNoite.getString("temp_max_tende_icone"));
                noite.setTemp_min_tende(objNoite.getString("temp_min_tende"));
                noite.setTemp_min_tende_icone(objNoite.getString("temp_min_tende_icone"));

                listPrevisao.add(manha);
                listPrevisao.add(tarde);
                listPrevisao.add(noite);

            } else {
                Previsao.UltimosDias ultimosDias = new UltimosDias();
                ultimosDias.setData(datas[i]);
                ultimosDias.setTurno("Previsão para o dia");
                ultimosDias.setResumo(objData.getString("resumo"));
                ultimosDias.setTemp_max(objData.getInt("temp_max"));
                ultimosDias.setTemp_min(objData.getInt("temp_min"));
                ultimosDias.setInt_vento(objData.getString("int_vento"));
                ultimosDias.setIcone(objData.getString("icone"));
                ultimosDias.setDiaDaSemana(objData.getString("dia_semana"));
                ultimosDias.setUmidade_max(objData.getInt("umidade_max"));
                ultimosDias.setUmidade_min(objData.getInt("umidade_min"));
                ultimosDias.setTemp_max_tende(objData.getString("temp_max_tende"));
                ultimosDias.setTemp_max_tende_icone(objData.getString("temp_max_tende_icone"));
                ultimosDias.setTemp_min_tende(objData.getString("temp_min_tende"));
                ultimosDias.setTemp_min_tende_icone(objData.getString("temp_min_tende_icone"));

                listPrevisao.add(ultimosDias);
            }
        }
        return listPrevisao;
    }
}
