package com.example.zinga.model;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class Estados {

    private String nome;
    private String sigla;
    private int id;
    private int identificador;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdentificador() {
        return identificador;
    }

    public void setIdentificador(int identificador) {
        this.identificador = identificador;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSigla() {
        return sigla;
    }

    public void setSigla(String sigla) {
        this.sigla = sigla;
    }

    @Override
    public String toString() {
        return "id: " + getId()
                + ", identificador: " + getIdentificador()
                + ", sigla: " + getSigla()
                + ", nome: " + getNome();
    }

    public List<Estados> jsonIterable(JSONArray jsonArray) throws Exception {
        List<Estados> listaEstados = new ArrayList<>();
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject obj = jsonArray.getJSONObject(i);

            Estados eb = new Estados();
            eb.setIdentificador(obj.getInt("id"));
            eb.setSigla(obj.getString("sigla"));
            eb.setNome(obj.getString("nome"));

            listaEstados.add(eb);
        }
        return listaEstados;
    }
}
