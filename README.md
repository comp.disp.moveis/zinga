# Trabalho Final Disciplina de Computação Para Dispositivos Móveis
## Ferramenta
A aplicação foi desenvolvida utilizando a IDE Android Stúdio em linguagem Java e emuladores executando o SO Android.

## Breve resumo da aplicação  
>Esta aplicação Android é responsável por consultar uma API com informações de clima e tempo. Exibir informações como precipitação de chuva, temperatura ambiente e velocidade do vento, etc.
>Foi elaborado sequindo a estrutura MVC (Model, View, Controller) e o SQLite para persistência de dados localmente.

## Desenvolvimento  
>Para iniciar o desenvolvimento, é necessário clonar o projeto num diretório de sua preferência. Para os desenvolvedores participantes deste projeto, é preferível que criem um branch para a codificação das respectivas funcionalidades.
>Para criar um branch, a melhor maneira é por linha de comando.
>Download do git aqui -> https://git-scm.com/download/win
>Comandos básicos do git aqui -> https://tableless.com.br/alguns-comandos-git/
### Passo-a-Passo básico
>1. Abra o terminal ou o git bash, se for no windows, no local onde deseja salvar o projeto clonado;
>2. Digite: "git clone https://gitlab.com/comp.disp.moveis/zinga.git";
>3. Abra o terminal dentro do diretório do projeto e crie um branch, digite: "git checkout -b nome-do-branch";
>4. Verifique se o branch foi criado e está selecionado para uso: "git branch";
>5. Para mudar de branch: "git checkout nome-do-branch";
>6. Sincronize todos os commits e branchs do repositório local com o remoto usando o: "git fetch";
### Para "subir" as modificações para o repositório remoto
>1. Adicionar arquivos a serem commitados: "git add . "
>2. Comitar os arquivos adicionados: "git commit -m "Observação-para-o-commit"
>3. Subir para o repositório remoto: "git push origin nome-do-branch"

